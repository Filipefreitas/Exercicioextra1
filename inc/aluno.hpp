#ifndef ALUNO_H
#define ALUNO_H
#include "pessoa.hpp"
using namespace std;
class Aluno: public Pessoa {

private:

	int matricula;

	string email;

	string curso;

	int semestre;

	int quantidade_creditos;
	
	float ira;

public:

Aluno();

Aluno(int matricula,string email,string curso,int semestre,int quantidade_creditos,float ira);
	
int getMatricula();	
		void setMatricula(int matricula);

string getEmail();	
		void setEmail(string email);

string getCurso();
	
		void setCurso(string curso);

float getIra();
		void setIra(float ira);

int getSemestre();
	
		void setSemestre(int semestre);

int getQuantidade_Creditos();

		void setQuantidade_Creditos(int quantidade_creditos);
};
#endif
